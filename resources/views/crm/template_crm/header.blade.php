
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Web POS MAJOO</title>

    <!-- Custom fonts for this template-->
    <link href="{{URL::to('/')}}/crm/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

         <!-- SweetAlert 2-->
    <link href="{{URL::to('/')}}/crm/vendor/sweetalert/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{URL::to('/')}}/crm/css/sb-admin-2.min.css" rel="stylesheet">


    <!-- Custom styles for this page -->
    <link href="{{URL::to('/')}}/crm/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{URL::to('/')}}/assets/css/jquery-ui.css') ?>">
    <script src="{{URL::to('/')}}/crm/js/jquery-3.3.1.js"></script>

    <link href="{{URL::to('/')}}/crm/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
<link href="{{URL::to('/')}}/crm/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<link href="{{URL::to('/')}}/crm/vendor/jquery-datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

</head>
