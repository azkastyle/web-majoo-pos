<?php

namespace App\Helpers;

/*
 * External Components Load
 */

// use Aws\S3\S3Client;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class Upload
{
    public static function name_format($name, $format, $size = '')
    {
        if ($size !== '') {
            $size = '_' . str_replace(',', '_', $size);
        }

        $slug     = Str::slug($name, '-');
        $sitename = env('APP_NAME');
        
        $merge    = $slug . $size . '.' . $format;
        $name     = strtolower($merge);

        return $name;
    }

    public static function path_date()
    {
        return date("Y") . "/" . date("m") . "/" . date('d') . "/";
    }

    public static function image_save($source, $name)
    {
        
        $datePath = Upload::path_date();
        $uniq     = uniqid() . '-';

        $originalPath  = public_path() . '/front/img/product/thumbnail/';
           
        if (!is_dir($originalPath)) {
            mkdir($originalPath, 0775, true);
        }
        
        // Change Image Name
        $nameImage = Upload::name_format($name, 'png');
        $nameImage = $uniq . $nameImage;
        
        // Get Image
        $imageContent   = base64_encode(file_get_contents($source));
        $image          = base64_decode($imageContent);
        $imageToStorage = Image::make($image);
        
        // Original Create Image
        $imageToStorage->save($originalPath . $nameImage);
        

        return $nameImage;
    }
}