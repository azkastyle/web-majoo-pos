<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@frontHome')->name('homeFront');

Route::get('crm_majoo', 'AuthController@loginPage')->name('loginPage');

Route::get('home_crm', 'AuthController@homeCRM')->name('homeCRM');

Route::post('login', 'AuthController@login')->name('login');

Route::get('listCategory', 'CategoryController@listCategory')->name('listCategory');

Route::get('/category/create', 'CategoryController@create')->name('formCreateCategory');

Route::get('/category/edit/{id}', 'CategoryController@edit')->name('editCategory');

Route::post('/category/save', 'CategoryController@store')->name('category.store');

Route::post('/category/put/{id}', 'CategoryController@update')->name('updateCategory');

Route::get('/category/delete/{id}', 'CategoryController@delete')->name('category.delete');

Route::get('listProduct', 'ProductController@listProduct')->name('listProduct');

Route::get('/product/delete/{id}', 'ProductController@delete')->name('product.delete');

Route::post('/product/save', 'ProductController@postProduct')->name('product.store');

Route::get('/product/create', 'ProductController@create')->name('formCreateProduct');

Route::get('/logout', 'AuthController@logout')->name('logout');