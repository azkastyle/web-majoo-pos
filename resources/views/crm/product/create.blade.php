<link href="{{URL::to('/')}}/assets/vendor/cropper/css/cropper.min.css" rel="stylesheet">
@include('crm.template_crm.header')
@include('crm.template_crm.sidebar')
 <!-- Begin Page Content -->
 <div class="container-fluid">

<!-- Page Heading -->
<div class="row">
	<div class="col-lg-0 mg-r-30">
        <a href="{{ route('listProduct') }}" class="btn btn-success btn-icon-split">
            <span class="icon text-white-600">
                <i class="fas fa-arrow-left"></i>
            </span>
        </a>
	</div><!-- col -->
    &nbsp;&nbsp;
    <h1 class="h3 mb-4 text-gray-800">Create Product</h1>
  </div><!-- row -->

                  @if(session('errors'))
                    <div class="alert alert-danger alert-dismissible show fade">
                      <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                          <span>×</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif

 <div class="col-xl-12 col-lg-12 col-md-12">

<div class="card o-hidden border-0 shadow-lg my-5">
<div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <form  enctype="multipart/form-data" method="POST" action="{{ route('product.store') }}">
                  @csrf
                   <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="Masukan Name">
                           
                    </div>
                  
                    <p>Kategori</p>
                    <div class="form-group">
                        <select id="category_id" name="id_kategori" class="form-control" data-placeholder="Choose Kategori">
                                  <option value=""><< Pilih Kategori Produk >></option>  
                                @foreach ($list_category as $value)
                                  <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" name="price" class="form-control" id="price" placeholder="Masukan Price"> 
                    </div>

                    <div class="form-group">
                            <label for="description">Description :</label>
                            <textarea id="mymce" name="description">{{ old('description') != null ? old('description') : '<strong>Majoo</strong> – ' }}</textarea>
                    </div>

                    <div class="form-group">
                            <label for="price">Stok</label>
                            <input type="text" name="stok" class="form-control" id="stok" placeholder="Masukan Stok"> 
                    </div>

                    <div class="form-group">
                            <label for="file">Gambar</label>
                            <input type="file" name="thumbnail" class="form-control" id="thumbnail" accept="image/png, image/jpeg">
                    </div>

  
                <button type="submit"  name="Submit" value="submit" class="btn btn-primary mt-3 mb-0">Submit</button>
                <hr>

                </form>
                
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- CKEDITOR js -->
<script src="{{URL::to('/')}}/crm/vendor/ckeditor/ckeditor.js"></script>
<script src="{{URL::to('/')}}/crm/vendor/tinymce/tinymce.min.js"></script>
<!-- Cropper-->
<script src="{{URL::to('/')}}/crm/vendor/cropper/js/cropper.min.js"></script>

<script>
$(document).ready(function(){
$("#menu-loker").addClass("active");
});

var description = document.getElementById("body");
  CKEDITOR.replace(description,{
    language:'en-gb'
  });
  CKEDITOR.config.allowedContent = true;

  $(function() {
    if ($("#mymce").length > 0) {
      tinymce.init({
        selector: "textarea#mymce",
        autosave_interval: "20s",
        extended_valid_elements : "vivaembed[instance|id|url|caption|source]",
        custom_elements: "p,vivaembed",
        theme: "modern",
        height: 550,
        plugins: [
          "advlist scayt autolink link image lists charmap print preview hr anchor pagebreak spellchecker autosave",
          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
          "save table contextmenu directionality emoticons template paste textcolor"
        ],
        // internalimage
        toolbar: "insertfile scayt undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image infografik | print preview fullpage paste | forecolor backcolor | internalimage youtube instagram twitter facebook vlix dailymotion joox soundcloud",
        contextmenu: "image link | paste copy cut",
        autosave_ask_before_unload: true,
        autosave_retention: "5m",
        scayt_customerId: '1:ZWE6y-mr9aj2-YWUQw2-kpEvJ2-5b7K8-FAdlI-2G4s5-mTvJu1-6460b1-gxPjw-SSQva3-rCf',
        scayt_autoStartup: true,
        scayt_slang: "id_ID",
      });
    }
  });


</script>

@include('crm.template_crm.footer')










