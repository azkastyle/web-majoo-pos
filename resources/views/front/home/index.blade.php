@extends('front.layouts.app-front')
@section('title','Home')

@section('content')
    <div class="container">
       <!-- BANNER MAJOO -->
        <section class="hero pb-3 bg-cover bg-center d-flex align-items-center" style="background: url({{URL::to('/')}}/front/img/majoo.jpg)">
        </section>
      
        <!-- DAFTAR PRODUCTS-->
        <section class="py-5">
          <header>
            <h2 class="h5 text-uppercase mb-4">Produk</h2>
          </header>
          <div class="row mb-3 align-items-center">
              <div class="col-lg-6 mb-2 mb-lg-0">
                  <p class="text-small text-muted mb-0">Showing {{ $list_produkPage->current_page }} to {{ $list_produkPage->per_page }} of {{ $list_produkPage->total }} entries</p>
                </div>
          </div>
          <div class="row">
            <!-- PRODUCT-->
            @foreach ($list_produk as $list )
            <div class="col-xl-3 col-lg-4 col-sm-6">
              <div class="product text-center">
                <div class="position-relative mb-3">
                  <div class="badge text-white badge-info">New</div><a class="d-block" href="#"><img class="img-fluid w-100" src="{{URL::to('/')}}/front/img/product/thumbnail/{{$list->thumbnail}}" alt="..."></a>
                 
                </div>
                <h6> <a class="reset-anchor" href="detail.html">{{$list->name}}</a></h6>
                <p class="small text-muted">Rp.{{number_format($list->price)}}</p>
                <p class="text-small mb-9">
                  <?php
                    echo strip_tags("$list->description");
                  ?>
                </p>
                <li class="list-inline-item m-0 p-0"><a class="btn btn-sm btn-dark" href="#">Beli</a></li>
              </div>
            </div>
            @endforeach
          </div>
           <!-- PAGINATION-->
           <nav aria-label="Page navigation example">
              <ul class="pagination justify-content-center justify-content-lg-end">
                  {{$list_produkPage->next_page_url }}
              </ul>
            </nav>
        </section>
  
      </div>
@endsection