<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;

use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function homeCRM(){
        if(!session('berhasil_login')){
            return view('crm.loginPage');
         }else{
            return view('crm.dashboard.index');
         }
    }
    public function logout(Request $request){
        $request->session()->flush();
        return view('crm.loginPage');
    }  
    public function loginPage(){
        return view('crm.loginPage');
    }
    public function login(Request $request){
        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->asForm()->post(env('API_LOGIN'), [
            'email' => $request->email,
            'password' => $request->password,
        ]);

        $responseBody = json_decode($response->getBody());

        if($responseBody->error == false){
            session(['berhasil_login' => true]);
            return redirect('home_crm');
        }else{
            Session::flash('errors', $responseBody->message);
            return redirect('/crm_majoo');
        }
    }
}
