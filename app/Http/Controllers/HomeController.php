<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use GuzzleHttp\Client;

class HomeController extends Controller
{
    public function frontHome(){
        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_GET_PRODUCT_ALL');

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        $list_produk = $responseBody->products->data;
        $list_produkPage = $responseBody->products;
        return view('front.home.index', compact('list_produk','list_produkPage'));
    }
}
