     <footer class="bg-dark text-white">
        <div class="container py-4">
              <div class="col-lg-12 text-lg-center">
                <p class="small text-muted mb-0"> <a class="text-white reset-anchor" href="https://majoo.id/">2022 &copy; PT Majoo Teknologi Indonesia</a></p>
              </div>
        </div>
      </footer>