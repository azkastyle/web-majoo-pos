<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    public function listCategory(){
        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_GET_CATEGORY');

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        $list_category = $responseBody->category;


        return view('crm.category.index', compact('list_category'));

    }

    public function delete(Request $request){
        $id = $request->id;

        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_DELETE_CATEGORY').$id;

        $response = $client->request('DELETE', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        if($responseBody->status == true){
            Session::flash('success', $responseBody->message);
        }else{
            Session::flash('errors', $responseBody->message);
        }

        return redirect()->route('listCategory');

    }

    public function create(){
        return view('crm.category.create');
    }

    public function edit(Request $request){
        $id = $request->id;
        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_GET_CATEGORY_BY_ID').$id;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        $kategori = $responseBody->category;

        // dd($list_category);


        return view('crm.category.edit', compact('kategori'));

    }

    public function store(Request $request){
        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->asForm()->post(env('API_STORE_CATEGORY'), [
            'name' => $request->name,
        ]);

        $responseBody = json_decode($response->getBody());

        if($responseBody->status == true){
            Session::flash('success', $responseBody->message);
        }else{
            if($responseBody->validasi != null){
                Session::flash('errors', $responseBody->validasi->name[0]);
            }else{
                Session::flash('errors', $responseBody->message);
            }
        }

        return redirect()->route('listCategory');

    }

    public function update(Request $request){
        $id = $request->id;

        $url = env('API_UPDATE_CATEGORY').$id;

        $response = Http::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->asForm()->post($url, [
            'name' => $request->name,
        ]);

        $responseBody = json_decode($response->getBody());

        if($responseBody->status == true){
            Session::flash('success', $responseBody->message);
        }else{
            if($responseBody->validasi != null){
                Session::flash('errors', $responseBody->validasi->name[0]);
            }else{
                Session::flash('errors', $responseBody->message);
            }
        }

        return redirect()->route('listCategory');


    }

}
