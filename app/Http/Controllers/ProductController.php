<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Http;

use App\Helpers\Upload;

use Illuminate\Support\Facades\Validator; 

use App\Product;

class ProductController extends Controller
{
    public function listProduct(){
        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_GET_PRODUCT_CRM');

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        $list_produk = $responseBody->products;
        return view('crm.product.index', compact('list_produk'));

    }

    public function create(){
        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_GET_CATEGORY');

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        $list_category = $responseBody->category;

        return view('crm.product.create', compact('list_category'));
    }

    public function postProduct(Request $request){

        $params = $request->all();

        $validasi  = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $data['name'] = trim($params['name']);
        $data['id_kategori'] = $params['id_kategori'];
        $data['price'] = $params['price'];
        $data['description'] = $params['description'];
        $data['stok'] = $params['stok'];
        
        if (isset($params['thumbnail'])) {
            $imageToStorage = Upload::image_save($params['thumbnail'], $params['name']);
            $data['thumbnail'] = $imageToStorage;
        }

        if($validasi->fails()){
            return redirect()->back()->withErrors($validasi)->withInput($request->all);
        }

        $save = Product::create($data);

        
        if ($save) {
            Session::flash('success', 'Success Create Produk');
        }else {
            Session::flash('errors', 'Gagal Create Payment');
        }
        
        return redirect()->route('listProduct');

    }

    public function delete(Request $request){
        $id = $request->id;

        $client = new Client(); //GuzzleHttp\Client
        $url = env('API_DELETE_PRODUCT').$id;

        $response = $client->request('DELETE', $url, [
            'verify'  => false,
        ]);

        $responseBody = json_decode($response->getBody());

        if($responseBody->status == true){
            Session::flash('success', $responseBody->message);
        }else{
            Session::flash('errors', $responseBody->message);
        }

        return redirect()->route('listProduct');

    }
}
