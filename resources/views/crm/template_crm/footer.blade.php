

<!-- Footer -->
<footer class="sticky-footer bg-white">
<div class="container my-auto">
<div class="copyright text-center my-auto">
    <span>Copyright &copy; PT Majoo Teknologi Indonesi</span>
</div>
</div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<script src="{{URL::to('/')}}/crm/vendor/jquery-datetimepicker/jquery.datetimepicker.full.min.js"></script>

<script src="{{URL::to('/')}}/crm/js/jquery-ui.js"></script>
<script src="{{URL::to('/')}}/crm/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{URL::to('/')}}/crm/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="{{URL::to('/')}}/crm/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{URL::to('/')}}/crm/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="{{URL::to('/')}}/crm/js/demo/datatables-demo.js"></script>

 <!-- SweetAlert 2-->
<script src="{{URL::to('/')}}/crm/vendor/sweetalert/sweetalert2.min.js"></script>
<script src="{{URL::to('/')}}/crm/vendor/sweetalert/sweetalert2.common.min.js"></script>

<script src="{{URL::to('/')}}/crm/plugins/momentjs/moment.js"></script>
<script src="{{URL::to('/')}}/crm/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#date_of_birth').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD', weekStart : 0, time: false });

      //blok sebelum hari ini
      // minDate: moment().add('d', 1).toDate(),

      $('#startDateReport').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD', weekStart : 0,time:false }).on('change', function(e, date)
        {
            $('#endDateReport').bootstrapMaterialDatePicker('setMinDate', date);

            //range hanya dalam satu bulan
            // $('#endDateReport').bootstrapMaterialDatePicker('setMaxDate',moment(date).add(30, 'day').format('YYYY-MM-DD'));
        });
      // $('#startDateReport').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD', weekStart : 0, time: false });
      $('#endDateReport').bootstrapMaterialDatePicker({
         format : 'YYYY-MM-DD',
         weekStart : 0, 
         time: false
        });
      // $('#date').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD', weekStart : 0, time: false, minDate : new Date() });
    });
  </script>

  <script type="text/javascript">
  function functionDelete(url){
        swal({
          title: 'Apakah anda yakin akan menghapus Data?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya, Hapus',
          cancelButtonText: 'Tidak, Kembali!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(function () {
            swal("Dihapus!", "Data Telah Dihapus!", "success");
            window.location = url;
        }, function (dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          if (dismiss === 'cancel') {
            swal("Cancelled", "Data tidak jadi dihapus", "error")
          }
        })
    }  
  </script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type="text/javascript"> 
    $(document).ready(function () {
        $('#table-report').DataTable({
            responsive: true,
            searching: true,
            dom: 'Bfrtip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
        }).order( [ 0, 'desc' ] ).draw();
    });

    $('#dataTable').DataTable({
        responsive: true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        }
      }).order( [ 0, 'desc' ] ).draw();
</script>

</body>

</html>