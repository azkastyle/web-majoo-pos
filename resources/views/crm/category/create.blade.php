@include('crm.template_crm.header')
@include('crm.template_crm.sidebar')
 <!-- Begin Page Content -->
 <div class="container-fluid">

<!-- Page Heading -->
<div class="row">
	<div class="col-lg-0 mg-r-30">
        <a href="{{ route('listCategory') }}" class="btn btn-success btn-icon-split">
            <span class="icon text-white-600">
                <i class="fas fa-arrow-left"></i>
            </span>
        </a>
	</div><!-- col -->
    &nbsp;&nbsp;
    <h1 class="h3 mb-4 text-gray-800">Create Category</h1>
  </div><!-- row -->

                  @if(session('errors'))
                    <div class="alert alert-danger alert-dismissible show fade">
                      <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                          <span>×</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif

 <div class="col-xl-12 col-lg-12 col-md-12">

<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
          <div class="col-lg-12">
              <div class="p-5">
                  <form  enctype="multipart/form-data" method="POST" action="{{ route('category.store') }}">
                    @csrf
                    <div class="form-group">
                              <label for="name">Name</label>
                              <input class="form-control" type="text" name="name" placeholder="Ex : name" required>
                    </div>

                  <button type="submit"  name="Submit" value="submit" class="btn btn-primary mt-3 mb-0">Submit</button>
                  <hr>

                  </form>
                  
              </div>
          </div>
      </div>
  </div>
</div>
</div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
 $(document).ready(function(){
    $("#menu-master").addClass("active");
  });
</script>

@include('crm.template_crm.footer')

