@include('crm.template_crm.header')
@include('crm.template_crm.sidebar')
  <!-- Begin Page Content -->
  <div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">List Product</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <a class="btn btn-info" href="{{ route('formCreateProduct') }}">Create</a>
      </div>

      @if(session('success'))
        <div class="alert alert-success alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>×</span>
            </button>
            {{session('success')}}
          </div>
        </div>
      @endif

      @if(session('errors'))
        <div class="alert alert-danger alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
            <span>×</span>
            </button>
            {{session('errors')}}
          </div>
        </div>
      @endif

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="wd-5p">ID</th>
                <th class="wd-25p">Name</th>
                <th class="wd-25p">Category</th>
                <th class="wd-25p">Image</th>
                <th class="wd-25p">Price</th>
                <th class="wd-25p">Stok</th>
                <th class="wd-25p">Description</th>
                <th class="wd-10p">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($list_produk as $value)
              <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->category }}</td>
                <td>
                  <img src="{{URL::to('/')}}/front/img/product/thumbnail/{{$value->thumbnail}}" alt="" title="" width="100px" height="100px">
                </td>
                <td>Rp.{{ number_format($value->price) }}</td>
                <td>{{ $value->stok }}</td>
                <td>
                    <?php
                        echo strip_tags("$value->description");
                    ?>
                </td>
                <td>
                  <a class='btn btn-danger btn-xs' onclick="functionDelete('{{ route('product.delete', $value->id) }}')" class=""><i class="fa fa-trash "></i> </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<script type="text/javascript">
  $(document).ready(function(){
    $("#menu-master").addClass("active");
  });

  function functionDelete(url){
    swal({
      title: 'Apakah Anda Yakin?',
      text: "Anda akan menghapus category ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus!',
      cancelButtonText: 'Tidak, gagalkan!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
      swal("Terhapus!", "Anda telah mengapus category", "success");
      window.location = url;
    }, function (dismiss) {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'cancel') {
        swal("Cancelled", "Anda tidak jadi menghapus category", "error")
      }
    });
  }
</script>
@include('crm.template_crm.footer')